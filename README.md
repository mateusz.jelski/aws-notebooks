# aws-notebooks

This repository contains notebooks for learning AWS basics.
They are meant to be edited during learning, to take notes and write down exercice results.

* [Intro](00-intro/) - learning goals (scope), conventions used, required tools
* [What is a Cloud](01-what-is-a-cloud/) - overview of Cloud Provider services, using the Web Console, the CLI and what is IAM
* [Compute services](02-compute-services/) - services to run applications
* [Exposing services](03-exposing-services/) - how to make applications available to anyone
* [Virtual Networks](04-virtual-networks/) - expose your applications in a secure way
* [Object storage](05-object-storage/) - simplest and cheapest way to store data
* [Key value storage](06-storage-services/) -
* [Relational databases](07-storage-services/) -
* [Serverless](08-serverless/) - run apps without managing servers
* [Automation](09-automation/) - manage cloud resources automatically
